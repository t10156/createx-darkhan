$(document).ready(function(){
    $('.bckgrnd_slick').slick({
      nextArrow: '.arrow_right',
      prevArrow: '.arrow_left',
      autoplay: true 
    });
  });
  $('.play').click(function(){
      $('.show').show()
  });
  $('.projects').slick({
    nextArrow: '.arrow_rightt',
    prevArrow: '.arrow_leftt',
    slidesToShow: 3,
    slidesToScroll: 1,
  });
  $('.sliders-5').slick({
    nextArrow: '.arrow_right_5',
    prevArrow: '.arrow_left_5',
  });
  $('.middle_cont_4').slick({
    nextArrow: '.left_arrows_4',
    prevArrow: '.right_arrows_4',
    slidesToShow: 3,
    slidesToScroll: 1,
  });
  $('.sliders_3').slick({
    nextArrow: '.left_arrows_3',
    prevArrow: '.right_arrows_3',
  });

  